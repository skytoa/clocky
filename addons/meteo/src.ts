import Addon from '../../src/model/Addon.ts'

export const name = 'Meteo'
export const author = 'Jeremy'
export const version = '0.1'

export function register(addon: Addon) {
    console.log('Hello world from meteo')

    addon.screen.pixels = addon.screen.pixels
        .map(column => column
            .map(pixels => pixels.map(pixel => 3)))

    addon.on('onScreenDisplay', e => e.preventDefault())

    setTimeout(async () => {
        console.debug('prio')
        const screen = await addon.requestPriorityDisplay()
        console.log('hey')
        screen.pixels = screen.pixels
            .map(column => column
                .map(pixels => pixels.map(pixel => 4)))
    }, 5_000);

}

