import Addon from '../../../src/model/Addon.ts'

export const name = 'Clock'
export const author = 'Jeremy'
export const version = '0.1'

export function register(addon: Addon) {
	console.log('Hello world from clock')

	addon.screen.pixels = addon.screen.pixels
		.map(column => column
			.map(pixels => pixels.map(pixel => 2)))
}