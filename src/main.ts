import { start as startWebApp } from './web/Web.ts'
import Screen from './model/Screen.ts'
import AddonManager from './model/AddonManager.ts'

const mainScreen = new Screen(3, 3)

const addonsManager = new AddonManager()
mainScreen.children.push(addonsManager.screen)

await addonsManager.registerAddons().catch(console.error)

startWebApp()

setInterval(() => {
    addonsManager.schedulerTick()
}, 10_000)

setInterval(() => {
    console.debug(mainScreen.applyChildren())
}, 2500)
