const DEFAULT_CONFIG = {
	addons: [
		'./addons/clock/src/main.ts'
	]
}

const CONFIG_FILE_NAME = 'config.json'
const CONFIG_FILE_PATH = './' + CONFIG_FILE_NAME

// TODO use promise to get config

export function writeConfigValue(path: string, value: any) {
	const config = getConfig()
	config[path] = value
	Deno.writeTextFileSync(CONFIG_FILE_PATH, JSON.stringify(config))
}

export function getConfigValue(path: string, defaultValue?: any) {
	const value = getConfig()[path]

	if (!value)
		writeConfigValue(path, defaultValue)

	return value ? value : defaultValue
}

function getConfig(): any {
	// Generate default config if require
	if (!Array
		.from(Deno.readDirSync(CONFIG_FILE_PATH + '/..'))
		.filter(e => e.name === CONFIG_FILE_NAME)
	) {
		Deno.writeTextFileSync(CONFIG_FILE_PATH, JSON.stringify(DEFAULT_CONFIG))
		console.log('Write default config file')
		return DEFAULT_CONFIG
	}

	return JSON.parse(Deno.readTextFileSync(CONFIG_FILE_PATH))
}