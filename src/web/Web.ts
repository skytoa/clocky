import { Application } from 'https://deno.land/x/abc/mod.ts'

const PORT_NUMBER = 8080

const webApp = new Application()
export default webApp

// This is a testing uri
webApp.get('/dummy', () => ({ hello: 'work' }))

// Register React application
webApp.static('/', './build')
    .file('/', './build/index.html')

export function start() {
    webApp.start({ port: PORT_NUMBER })
    console.log('Web server started on port ' + PORT_NUMBER)
}