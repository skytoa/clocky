export default class Screen {
	height: number
	width: number

	xOrigin: number
	yOrigin: number

	pixels: number[][][]
	children: Screen[]

	constructor(height: number, width: number, xOrigin = 0, yOrigin = 0) {
		this.height = height
		this.width = width

		this.xOrigin = xOrigin
		this.yOrigin = yOrigin

		this.pixels = [...Array(width)]
			.map((_) =>
				[...Array(height)]
					.map((_) => [xOrigin, 0, 0])
			)

		this.children = []
	}

	public applyChildren(): number[][][] {
		// copy, Filter, apply, return
		const view = this.pixels.slice(0, this.height)

		this.children.forEach((child) => {
			child.applyChildren()
				// Filter out of range pixel
				.slice(0, this.height - child.xOrigin)
				.map((column) => column.slice(0, this.width - child.yOrigin))
				// Apply child view to current view
				.forEach((column, x) =>
					column.forEach((pixel, y) =>
						view[x + child.xOrigin][y + child.yOrigin] = pixel
					)
				)
		})

		return view
	}
}
