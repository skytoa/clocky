import Screen from './Screen.ts'

export default class PriorityRequest {

    screen: Screen
    callback: (screen: Screen) => void
    isStarted: boolean
    startTime: number

    constructor(screen: Screen, callback: (screen: Screen) => void) {
        this.screen = screen
        this.callback = callback
        this.isStarted = false
        this.startTime = 0
    }

    public start() {
        this.isStarted = true
        this.startTime = Date.now()
        this.callback(this.screen)
    }

    public isPriorityComplete() {
        return this.isStarted && Date.now() - this.startTime > 30_000
    }

}