import { getConfigValue } from '../config.ts'
import Addon, { AddonModule } from './Addon.ts'
import Screen from './Screen.ts'
import { OnScreenDisplayEvent } from '../events/OnScreenDisplay.ts'
import { OnRequestPriorityEvent } from '../events/OnRequestPriority.ts'
import PriorityRequest from './PriorityRequest.ts'

export default class AddonManager {

	addons: Addon[] = []
	screen: Screen

	private currentAppIndex = 0
	private priorityQueue: PriorityRequest[] = []

	constructor() {
		this.screen = new Screen(3, 3)
	}

	async registerAddons() {
		const modules: AddonModule[] = await Promise.all(
			getConfigValue('addons')
				.map((path: string) => import(path) // Todo securise import
					.catch(console.error)
				))

		this.addons = modules
			.filter(mod => mod)
			.map((module) => new Addon(module))

		this.addons.forEach(addon => {
			addon.addEventListener('onRequestPriority', e => this.addonRequestPriority(e))
		})
	}

	public schedulerTick() {
		console.debug('App manager tick')

		if (this.priorityQueue[0]) {
			// Priority process
			const requets = this.priorityQueue[0]

			if (!requets.isStarted) {
				requets.start()
				this.screen.children = [requets.screen]
			} else if (requets.isPriorityComplete()) {
				this.priorityQueue = this.priorityQueue.slice(1)
				this.schedulerTick()
			}

		} else {
			// Round robin process
			this.currentAppIndex = (this.currentAppIndex + 1) % this.addons.length
			while (!this.addons[this.currentAppIndex]?.dispatchEvent(new OnScreenDisplayEvent()))
				this.currentAppIndex = (this.currentAppIndex + 1) % this.addons.length

			this.screen.children = [this.addons[this.currentAppIndex]?.screen]

		}
	}

	private addonRequestPriority(e: Event) {
		const event = e as OnRequestPriorityEvent
		this.priorityQueue = [new PriorityRequest(new Screen(3, 3), event.callbackFunction)]
	}
}
