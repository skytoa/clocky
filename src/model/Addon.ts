import Screen from './Screen.ts'
import { OnRequestPriorityEvent } from '../events/OnRequestPriority.ts'

export interface AddonModule {
	register: (addon: Addon) => void
	name?: string
	author?: string
	version?: string
}

export default class Addon extends EventTarget {

	name: string
	author: string
	version: string
	screen: Screen

	on = this.addEventListener

	constructor(module: AddonModule) {
		super()

		this.name = module.name ? module.name : 'Undefined'
		this.author = module.author ? module.author : 'Undefined'
		this.version = module.version ? module.version : 'Undefined'
		this.screen = new Screen(3, 3)

		module.register(this)
		console.log('Load module ' + this.name + ':' + this.version + ' from ' + this.author)
	}

	public async requestPriorityDisplay() {
		return new Promise((resolve: (screen: Screen) => any) => {
			this.dispatchEvent(new OnRequestPriorityEvent(resolve))
		})
	}
}