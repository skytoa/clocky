export class OnScreenDisplayEvent extends Event {

    constructor() {
        super('onScreenDisplay', { cancelable: true })
    }

}