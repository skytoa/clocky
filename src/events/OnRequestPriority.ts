export class OnRequestPriorityEvent extends Event {

    callbackFunction: any

    constructor(callbackFunction: any) {
        super('onRequestPriority', { cancelable: true })

        this.callbackFunction = callbackFunction
    }

}